Rails.application.routes.draw do
  get 'list', to: 'home#list'
  get 'pickup', to: 'home#pickup'

  root to: 'home#index'

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
