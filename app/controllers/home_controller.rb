# Home controller class
class HomeController < ApplicationController
  def index
    # Nothing to do
  end

  def list
    exts = %w[png jpg jpeg gif webp]
    base_path = Pathname('images')
    images_path = Rails.public_path.join(base_path)
    patterns = exts.map { |ext| "**/*.#{ext}" }
    files = Dir.glob(patterns, base: images_path).shuffle
    # files.map! { |f| URI.encode_www_form_component(f) }
    json = {}
    json[:files] = files
    render json:
  end

  def pickup
    exts = %w[png jpg jpeg gif webp]
    # exts = %w[jpg jpeg]
    base_path = Pathname('images')
    images_path = Rails.public_path.join(base_path)
    patterns = exts.map { |ext| "**/*.#{ext}" }
    file = Dir.glob(patterns, base: images_path).sample
    # redirect_to File.join('/images', file), allow_other_host: true
    send_data Rails.public_path.join('images', file).read, type: 'image/jpeg', disposition: 'inline'
  end
end
